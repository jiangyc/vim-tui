" load init file
let s:current_path = expand('<sfile>:p:h')

execute('source ' . s:current_path . (has('win32') ? '\init.vim' : '/init.vim'))
