
fu! tui#core#layer#load(layer_id)
    if !exists('g:tui_layers')
        call tui#core#layer#init()
    endif

    " 拼接基本路径
    let relative_path = g:tui_layers['path']
    if !empty(a:layer_id)
        " 将layer_id中的“.”替换为路径隔符，并拼接到默认路径后
        let relative_path = substitute(a:layer_id, '\.', tui#util#path#sep(), 'g')
        let relative_path = tui#util#path#join(g:tui_layers['path'], relative_path)
    endif

    " 获取真实路径
    let real_path = ''
    if filereadable(relative_path . '.vim')
        " 1、是文件（ + .vim是文件）
        let real_path = relative_path . '.vim'
    elseif isdirectory(relative_path)
        " 2、是目录
        if filereadable(relative_path . tui#util#path#sep() . 'init.vim')
            let real_path = relative_path . tui#util#path#sep() . 'init.vim'
        else
            let real_path = relative_path
        endif
    endif

    " 加载
    if !empty(real_path)
        if filereadable(real_path)
            execute('source ' . real_path)
        else
            let sub_files = globpath(real_path, '*.vim', 0, 1)
            for sub_file in sub_files
                execute('source ' . sub_file)
            endfor
        endif
    endif
endfu

" 功能：初始化layer配置，并加载默认的layer
fu! tui#core#layer#init(...)
    " 加载默认的layers路径
    let g:tui_layers = {"path": tui#util#path#join(tui#util#path#rtp(), 'layers')}
    if a:0 > 0 && type(a:1) == 1 && isdirectory(expand(a:1))
        let g:tui_layers = {"path": expand(a:1)}
    endif

    " 注册命令：TuiLayer
    command! -nargs=1 TuiLayer call tui#core#layer#load(<f-args>)
    " 加载Layer
    call tui#core#layer#load('')
endfu
