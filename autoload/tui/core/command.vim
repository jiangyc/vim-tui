" ... => is_cover
fu! tui#core#command#add(command_id, command_action, ...)
    if !exists('g:tui_commands')
        call tui#core#command#init()
    endif
    if empty(a:command_id) || empty(a:command_action)
        echoerr 'invalid command: ' . a:command_id . ' => ' . a:command_action
        return
    endif
    
    if !has_key(g:tui_commands, a:command_id) || (a:0 > 0 && a:1)
        let g:tui_commands[a:command_id] = a:command_action
    endif
endfu

fu! tui#core#command#exec(...)
    if !exists('g:tui_commands')
        call tui#core#command#init()
    endif
    if a:0 == 0
        for command_id in keys(g:tui_commands)
            echo command_id . ' => ' . g:tui_commands[command_id]
        endfor
    else
        if !has_key(g:tui_commands, a:1)
            echoerr 'invalid command: ' . a:1
        else
            execute(g:tui_commands[a:1])
        endif
    endif
endfu

fu! tui#core#command#init()
    " command_info: id、action
    let g:tui_commands = {}

    " 定义命令
    command! -nargs=? TuiCommand call tui#core#command#exec(<f-args>)
endfu
