fu! tui#widget#menubar#has_menu(menu_id)
    return !exists('g:tui_menubar') ? 0 : has_key(g:tui_menubar, a:menu_id)
endfu

fu! tui#widget#menubar#has_menu_item(menu_id, item_id)
    if !exists('g:tui_menubar') || !has_key(g:tui_menubar, a:menu_id)
        return 0
    endif
    return has_key(g:tui_menubar[a:menu_id]['items'], a:item_id)
endfu

fu! tui#widget#menubar#add_menu(menu_info)
    if !exists('g:tui_menubar')
        return
    endif
    if !has_key(a:menu_info, "id") || type(a:menu_info["id"]) != 1 || empty(a:menu_info["id"])
        echoerr "The menu ID cannot be omitted"
        return
    endif

    let menu_id = a:menu_info["id"]
    let menu_order = has_key(a:menu_info, "order") && type(a:menu_info["order"]) == 0 ? a:menu_info['order'] : 9999
    let menu_text = has_key(a:menu_info, "text") && !empty(a:menu_info["text"]) ? a:menu_info['text'] : menu_id
    if !tui#widget#menubar#has_menu(menu_id)
        let g:tui_menubar[menu_id] = {"id": menu_id, "text": menu_text, "order": menu_order, 'items': {}}
    endif
    
    if has_key(a:menu_info, 'items') && type(a:menu_info['items']) == 3
        for item_info in a:menu_info['items']
            call tui#widget#menubar#add_menu_item(menu_id, item_info)
        endfor
    endif
endfu

fu! tui#widget#menubar#add_menu_item(menu_id, item_info)
    if !exists('g:tui_menubar')
        return
    endif
    if !has_key(a:item_info, "id") || type(a:item_info["id"]) != 1 || empty(a:item_info["id"])
        echoerr "The menu item ID cannot be omitted"
        return
    endif

    let item_id = a:item_info["id"]
    let item_order = has_key(a:item_info, "order") && type(a:item_info["order"]) == 0 ? a:item_info['order'] : 9999
    let item_text = has_key(a:item_info, "text") && !empty(a:item_info["text"]) ? a:item_info['text'] : item_id
    let item_command = has_key(a:item_info, "command") && !empty(a:item_info["command"]) ? a:item_info['command'] : ''
    let item_tooltip = has_key(a:item_info, "tooltip") && !empty(a:item_info["tooltip"]) ? a:item_info['tooltip'] : ''
    if !tui#widget#menubar#has_menu_item(a:menu_id, item_id)
        let menu_items = g:tui_menubar[a:menu_id]['items']
        let menu_items[item_id] = {'id': item_id, 'menu_id': a:menu_id, 'text': item_text, 'order': item_order, 'tooltip': item_tooltip, 'command': item_command}
    endif
endfu

fu! tui#widget#menubar#compare(var_a, var_b)
    let order_a = has_key(a:var_a, 'order') ? a:var_a.order : 9999
    let order_b = has_key(a:var_b, 'order') ? a:var_b.order : 9999
    return order_a > order_b ? 1 : order_a < order_b ? -1 : 0
endfu

fu! tui#widget#menubar#reload()
    let menus = []
    for menu_id in keys(g:tui_menubar)
        call add(menus, g:tui_menubar[menu_id])
    endfor
    call sort(menus, 'tui#widget#menubar#compare')
    for menu_info in menus
        let menu_id = menu_info["id"]
        let menu_text = menu_info["text"]
        let menu_items = []
        for item_id in keys(menu_info['items'])
            call add(menu_items, menu_info['items'][item_id])
        endfor
        call sort(menu_items, 'tui#widget#menubar#compare')
        let quickui_menus = []
        for item_info in menu_items
            let item_id = item_info["id"]
            let item_text = item_info["text"]
            let item_command = item_info["command"]
            let item_tooltip = item_info["tooltip"]

            if item_text == "--"
                call add(quickui_menus, ["--", ""])
            else
                let temp_item_info = [item_text]
                " command
                if !empty(item_command)
                    if stridx(item_command, "#") == 0
                        call add(temp_item_info, substitute(item_command, '^#', "call ", ''))
                    else
                        call add(temp_item_info, item_command)
                    endif
                else
                    call add(temp_item_info, "")
                endif
                " tooltip
                if !empty(item_tooltip)
                    call add(temp_item_info, item_tooltip)
                else
                    call add(temp_item_info, '')
                endif
                call add(quickui_menus, temp_item_info)
            endif
        endfor
        " install menu
        call quickui#menu#install(menu_text, quickui_menus)
    endfor
endfu

fu! tui#widget#menubar#init()
    " tui_menubar: {menu_id: menu_info}
    " menu_info: {'menu_file': {'id': 'menu_file', 'text': '&File', 'order': 9999, items: {}}}
    " item_info: {'id': 'menu_item_file_exit', 'menu_id': 'menu_file', 'text': 'E&xit', 'command': '', 'tooltip': '', 'order': 9999}
    let g:tui_menubar = {}
    call quickui#menu#reset()

    " 快捷键绑定
    let g:quickui_border_style = 3
    let g:quickui_show_tip = 1
    noremap <space><space> :call quickui#menu#open()<cr>
endfu
