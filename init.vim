" ========== init runtime path ==========
let g:tui_rtp = expand('<sfile>:p:h')
if index(split(&rtp, ','), g:tui_rtp) == -1
    execute('set rtp+=' . g:tui_rtp)
endif

" ========== init layers ==========
" order: 1. parameter, 2. ./layers
call tui#core#layer#init()
