" ====================
"  Vim基本配置
" ====================
"
" 不兼容Vi
set nocompatible

" 文件类型及缩进
filetype on
filetype plugin on
filetype indent on
" 打开 C/C++ 语言缩进优化
set cindent

" 开启语法高亮
syntax on

" 编码设置
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
" set helplang=cn
" language messages zh_CN.utf-8
" vim的菜单乱码解决
" source $VIMRUNTIME/delmenu.vim
" source $VIMRUNTIME/menu.vim
" 打开文件自动检测的编码列表
set fileencodings=UTF-8,GB2312,GBK,UCS-BOM,CP936,LATIN-1

" 折叠
" zM/zm 关闭所有折叠（嵌套/不嵌套）
" zR/zr 打开有折叠（嵌套/不嵌套）
" za 打开/关闭折叠
if has('folding')
	" 允许代码折叠
	set foldenable
	" 代码折叠默认使用缩进
	set fdm=indent
	" 默认打开所有缩进
	set foldlevel=99
endif

" 开启行号显示
set number
" 开启相对行号
set relativenumber
" 高亮当前行
set cursorline
" 不生成以"~"结为的备份文件
set nobackup
" 不产生Swap文件
set noswapfile
" 不自动换行
set nowrap
set linebreak
" Tab宽度
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab " 用空格代替Tab
" 使用系统剪切板
set clipboard+=unnamed
" 命令菜单
set wildmenu
" 退格键删除文本而非移动光标
set backspace=2
" 设置当文件在外部被修改自动更新该文件
" set autoread
" 高亮显示查找结果
set hlsearch

" 启用终端色彩
set termguicolors

" inoremap <C-h> <left>
" inoremap <C-j> <down>
" inoremap <C-k> <up>
" inoremap <C-l> <right>
" inoremap <c-a> <home>
" inoremap <c-e> <end>
