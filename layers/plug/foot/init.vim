
" ========== 结束插件 ==========
call plug#end()

" ========== 执行钩子 ==========
" 判断插件是否启用
fu! g:PlugExists(plug_id)
    let plug_data_path = tui#util#path#join(g:tui_layers['vim-plug']['path'], a:plug_id)
    return index(g:plugs_order, a:plug_id) >= 0 && isdirectory(plug_data_path)
endfu
" 执行钩子
for plug_id in keys(g:tui_layers['vim-plug']['hooks'])
    let plug_hooks = g:tui_layers['vim-plug']['hooks'][plug_id]
    for HookFuncName in plug_hooks
        if exists('*' . HookFuncName . '()') && g:PlugExists(plug_id)
            execute('call ' . HookFuncName . '()')
        endif
    endfor
endfor

" ========== Tui命令 ==========
TuiLayer plug.foot.command
TuiLayer plug.foot.menubar
