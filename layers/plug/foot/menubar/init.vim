call tui#widget#menubar#init()

" menu_file
call tui#widget#menubar#add_menu({'id': 'menu_file', 'text': '&File', 'order': 100})
call tui#widget#menubar#add_menu_item('menu_file', {'id': 'item_file_exit', 'text': 'E&xit', 'command': 'q', 'tooltip': 'exit :q', 'order': 100})

" load
call tui#widget#menubar#reload()
