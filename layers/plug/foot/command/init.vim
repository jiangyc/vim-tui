call tui#core#command#init()

" encoding
if g:PlugExists('vim-fencview')
    call tui#core#command#add('encoding', 'FencView')
endif

if g:PlugExists('coc.nvim') && exists('g:coc_global_extensions')
    " explorer
    if index(g:coc_global_extensions, 'coc-explorer') >= 0
        call tui#core#command#add('explorer', 'CocCommand explorer')
    endif

    " terminal
    if index(g:coc_global_extensions, 'coc-terminal') >= 0
        call tui#core#command#add('terminal', 'call g:ToggleCocTerminal()')
    endif
endif

fu! g:ToggleCocTerminal()
    execute('CocCommand terminal.Destroy')
    execute('CocCommand terminal.Toggle')
endfu
