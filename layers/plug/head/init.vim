
" ========== 插件路径 ==========
let g:tui_layers['vim-plug'] = {'path': tui#util#path#join(tui#util#path#rtp(), 'plugged'), 'hooks': {}}

" ========== 启用插件 ==========
call plug#begin(g:tui_layers['vim-plug']['path'])

" ========== 钩子函数 ==========
fu! g:PlugHook(plug_id, hook_fun)
    let plug_hooks = has_key(g:tui_layers['vim-plug']['hooks'], a:plug_id) ? g:tui_layers['vim-plug']['hooks'][a:plug_id] : []
    if index(plug_hooks, a:hook_fun) == -1
        call add(plug_hooks, a:hook_fun)
    endif
    let g:tui_layers['vim-plug']['hooks'][a:plug_id] = plug_hooks
endfu
