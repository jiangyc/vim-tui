" vim
call insert(g:coc_global_extensions, 'coc-vimlsp') 

" json
call insert(g:coc_global_extensions, 'coc-json') 

" sh
call insert(g:coc_global_extensions, 'coc-sh') 

" powershell
" call insert(g:coc_global_extensions, 'coc-powershell') 

" html
" call insert(g:coc_global_extensions, 'coc-html') 

" css
" call insert(g:coc_global_extensions, 'coc-css') 

" js/ts
" call insert(g:coc_global_extensions, 'coc-tsserver') 

" cpp
" call insert(g:coc_global_extensions, 'coc-clangd') 

" rust
" call insert(g:coc_global_extensions, 'coc-rust-analyzer') 

" java
" call insert(g:coc_global_extensions, 'coc-java') 

" lua
" call insert(g:coc_global_extensions, 'coc-lua') 

" python
" call insert(g:coc_global_extensions, 'coc-pyright') 
