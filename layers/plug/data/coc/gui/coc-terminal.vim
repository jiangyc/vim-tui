call insert(g:coc_global_extensions, 'coc-terminal') 

fu! TuiTerminalToggle()
    CocCommand terminal.Destroy
    CocCommand terminal.Toggle
endfu
