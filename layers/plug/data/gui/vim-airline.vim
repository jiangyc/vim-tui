Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call g:PlugHook('vim-airline', 'Hook_vim_airline')

fu! Hook_vim_airline()
    " 启用状态栏
    set laststatus=2
    " Powerline字体
    let g:airline_powerline_fonts = 1
    " 显示tab
    let g:airline#extensions#tabline#enabled = 1
    let g:airline#extensions#tabline#buffer_nr_show = 1
     " 关闭状态显示空白符号计数,这个对我用处不大"
    let g:airline#extensions#whitespace#enabled = 0
    let g:airline#extensions#whitespace#symbol = '!'
    " 设置Airline主题
    let g:airline_theme='molokai'
endfu
