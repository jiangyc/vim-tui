Plug 'flazz/vim-colorschemes'
call g:PlugHook('vim-colorschemes', 'Hook_vim_colorschemes')

fu! Hook_vim_colorschemes()
    colorscheme molokai
endfu
