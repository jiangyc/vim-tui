Plug 'Yggdroot/indentLine'
call g:PlugHook('indentLine', 'Hook_indentLine')

fu! Hook_indentLine()
    let g:indentLine_color_term = 239
    let g:indentLine_color_gui = '#A4E57E'
    let g:indentLine_color_tty_light = 7
    let g:indentLine_color_dark = 1
    set conceallevel=1
    let g:indentLine_conceallevel = 2
    " Change Indent Char
    let g:indentLine_char = '|'
    " let g:indentLine_char_list = ['|', '¦', '┆', '┊']
    " 禁用针对JSON的双引号问题
    let g:vim_json_conceal = 0
endfu
