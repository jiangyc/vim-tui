# vim-tui

A vim tui configuration based on the vim-quickui project.

## Usage

1. obtain this configuration

```sh
# vim
git clone https://gitlab.com/jiangyc/vim-tui.git ~/.vim
# neovim
git clone https://gitlab.com/jiangyc/vim-tui.git ~/.config/nvim
```

2. install plugin

```sh
# open vim or nvim and execute this command
:PlugInstall
```

## Dependencies

- vim-plug - https://github.com/junegunn/vim-plug

- coc.nvim - https://github.com/neoclide/coc.nvim

- vim-quickui - https://github.com/skywind3000/vim-quickui
